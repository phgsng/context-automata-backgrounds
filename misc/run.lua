--
--------------------------------------------------------------------------------
--         FILE:  run.lua
--        USAGE:  texlua run.lua
--  DESCRIPTION:  Game of Life CLI frontend
-- REQUIREMENTS:  texlua or similar interpreter
--         BUGS:  dozens, if lucky
--       AUTHOR:  Philipp Gesang (Phg), <megas.kapaneus@gmail.com>
--      VERSION:  1.0
--      CREATED:  05/08/10 13:09:52 CEST
--------------------------------------------------------------------------------
--

-- check for a capable interpreter
if not (arg[-1] == "texlua" or 
        context ~= nil)     then
    print ([[

···············································································
Please use the LuaTeX interpreter or modify the sources to fit your
Lua machine of choice!
GOTO http://www.luatex.org/
···············································································
]])
    return 1
end

mplife = {}
mplife.setup = {}

require "automata-life"
require "automata-eca"

local C, Cs, Ct, P, R, S, match = lpeg.C, lpeg.Cs, lpeg.Ct, lpeg.P, lpeg.R, lpeg.S, lpeg.match

local help = gol.helpers

debug = 1

function get_args ()
    gol.arg = arg
    if gol.arg[-1] == "texlua" then
        gol.machine = gol.arg[-1]
        gol.self_name = gol.arg[0]
        gol.arg[-1], gol.arg[0] = nil, nil
    elseif context ~= nil then
        -- TODO
    end

    local kv_args = function ()
        local tmp = {}
        local so = help.split_once
        for _, a  in ipairs(gol.arg) do
            local lr = so(a, "=") 
            tmp[help.strip(lr[1], "-")] = lr[2]
        end
        return tmp
    end

    return kv_args()
end

function life (current)
    -- how many frames should we draw?
    current.n       = current.kv_args.n       or 40

    -- sustaining dead cells
    current.sustain = current.kv_args.sustain or 0 -- TODO
    current.fadeout = current.kv_args.fadeout or false 

    -- check for debug flag
    if tonumber(current.kv_args.debug) == 0 then
        debug = false
    else
        debug = current.kv_args.debug or debug
    end


    -- prepare the rule
    if current.kv_args.rule then
        current.rule = gol.parse_rule (current.kv_args.rule)
    else
        current.rule = gol.parse_rule ("B3/S23") -- Conway's rule
    end

    if debug then for n, a in pairs(current.kv_args) do print(n, a)  end end
    if debug then for i, j in pairs(current.rule)    do print(i, #j) end end

    -- read the initial state (get an array of strings)
    if current.file then
        current.init = gol.parse_file (current.file)
    else
        return 1
    end

    if current.init then
        if debug then 
            gol.pre_frame(current.init)
        end
        local many = gol.frames( current.init, current.rule, current.n )
        gol.pre_movie (many, true)
    else
        io.write"\nCheck your input file for consistency, please.\n"
        return 1
    end
    return 0
end

function elem(current)
    print (current.n, type(current.n))
    for n=1,current.n,1 do
        gol.pre_section(current.from:len(), n)
        eca.successive(current)
    end
    return 0
end

function main ()
    local current = {}
    current.kv_args = get_args()
    current.file = current.kv_args.file

    local p_suf = P"." * (1-P".")^3 * -P(1)
    local p_fn  = (1-p_suf)^1 * C(p_suf)
    local suffix = p_fn:match(current.file) or ".gol" -- assume .gol format as default

    if suffix == ".eca" then
        require "automatarc-eca"
        for i,j in pairs(mplife.setup.rc) do 
            current[i] = mplife.setup.rc[i]
        end
           current.aspect = tonumber(current.kv_args.aspect) or 3/4
             current.rule = eca.gen_rule(tonumber(current.kv_args.rule) or 110)
             current.from = eca.parse_file(current.kv_args.file)
        current.framesize = math.floor(current.aspect * current.from:len())

                current.n = tonumber(current.kv_args.n)
    end

    current.n = tonumber(current.kv_args.n)

    if suffix == ".eca" then
        mplife.setup.current = current
        return elem(current)
    else
        return life(current)
    end
end

if not context then
    return main()
end

