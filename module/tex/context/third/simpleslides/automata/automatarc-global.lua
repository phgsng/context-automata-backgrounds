--
--------------------------------------------------------------------------------
--         FILE:  automatarc-global.lua
--        USAGE:  with ConTeXt
--  DESCRIPTION:  default settings for the automaton module
--       AUTHOR:  Philipp Gesang (Phg), <megas.kapaneus@gmail.com>
--      VERSION:  1.0
--      CREATED:  13/08/10 10:35:46 CEST
--------------------------------------------------------------------------------
--

if not mplife then return 1 end

mplife.setup.global = {}

do
    local c = {}

--        c.file = "sships.gol"
--        c.init = gol.parse_file(c.file) 
--        c.last = c.init -- for successive mode

          c.rule = gol.parse_rule("B3/S23") -- default Conway

        c.aspect = 3/4 -- actually 1/(x/y), for eca mode

      c.extendxy = 0
       c.extendx = 0
       c.extendy = 0

       c.pensize = .1
         c.color = { r = .6, g = .6, b = .8 }
       c.opacity = 1/3
          c.fade = true
       c.gestalt = "unitsquare" -- try "unitcircle"

    c.firstframe = 1
        c.frames = 5

      c.preamble = [[
\setupcolors[state=start]
\setupbackgrounds[state=start]
\setuppapersize[S6][S6]

\setuppagenumbering[state=stop,location=]

\defineoverlay[back][\ctxlua{mplife.successive()}]

\setupbackgrounds [page] [background=back]
]]
  c.before_frame = [[
\startMPcode
pickup pencircle xyscaled (.25*]] .. c.pensize .. [[pt, ]] .. c.pensize .. [[pt) rotated 45;
]]
   c.after_frame = [[
currentpicture := currentpicture xysized (\the\paperwidth, \the\paperheight);
\stopMPcode
]]

    mplife.setup.global = c
end

return mplife.setup.global
