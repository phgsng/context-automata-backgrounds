if not mplife then return 1 end

mplife.setup.rc = {}

do
    local c = {}

    c.rule = eca.gen_rule(110)
    c.n    = 40

    c.from = ""

    c.clip = "left"

    c.aspect = 3/4 -- default for S6 slide format

    c.pensize = .1
    c.color = { r = .6, g = .6, b = .8 }
    c.opacity = 1/3
    c.fade = true
    c.gestalt = "unitsquare" -- try "unitcircle"

    c.firstframe = 1
    c.frames = 5

    c.preamble = [[
\setupcolors[state=start]
\setupbackgrounds[state=start]
\setuppapersize[S6][S6]

\setuppagenumbering[state=stop,location=]

\defineoverlay[back][\ctxlua{mplife.successive()}]

\setupbackgrounds [page] [background=back]
]]
    c.before_frame = [[
\startMPcode
pickup pencircle xyscaled (.25*]] .. c.pensize .. [[pt, ]] .. c.pensize .. [[pt) rotated 45;
]]
    c.after_frame = [[
currentpicture := currentpicture xysized (\the\paperwidth, \the\paperheight);
\stopMPcode
]]

    mplife.setup.rc = c
end

return mplife.setup.rc
